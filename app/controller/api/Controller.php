<?php 
namespace App\Controller\Api;

use App\Controller\Lib\CommonControl;
use App\Controller\Lib\Request;
use App\Controller\Lib\Response;
use App\Controller\Lib\Token;
use App\Model\DB;

/**
* 
*/
class Controller extends CommonControl
{
	
    private $is_auth;
    private $is_permit = true;
    private $token;
    private $jwt;

	protected function checkAuth($status = null){
        $this->token = $this->getAuth();
        $this->is_auth = Token::isValid($this->token);

        if($this->is_auth){
            $this->jwt = Token::decode($this->token);

            if(!$this->checkPermit($status))
                $this->is_permit = false;
        }
	}

    protected function isAuth(){
        return $this->is_auth;
    }

    protected function isPermit(){
        return $this->is_permit;
    }

    protected function getCurrentUser(){
        return $this->jwt->id;
    }

    protected function getCurrentToken(){
        return $this->token;
    }

    protected function getCurrentStatus(){
        return $this->jwt->status;
    }

    protected function getJWT(){
        return $this->jwt;
    }

    protected function checkPermit($status = null){
        if(!$this->jwt->is_active)
            return false;

        $is_valid = true;
        foreach ((array) $status as $key => $values) {
            if(isset($this->jwt->$key)){
                foreach ((array) $values as $value){
                    if($is_valid = ($this->jwt->$key === $value))
                        break;
                }
            }
            
        }

        return $is_valid;
    }
}