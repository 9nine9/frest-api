<?php 
namespace App\Controller\Api\Farmer;

use App\Controller\Api\Controller;
use App\Controller\Lib\Request;
use App\Controller\Lib\Response;
use App\Controller\Lib\Token;
use App\Model\DB;

/**
* 
*/
class FarmerController extends Controller
{

	public function __construct(){
        $this->checkAuth(['status' => 'f']);
	}

    public function updateSensorValue(Request $request){
        if(!$this->isAuth())
            return Response::error($this->error_invalid_token, 401);
        else if(!$this->isPermit())
            return Response::error($this->error_not_permit, 500);

        $sensor_id = (int) $request->post('sensor_id')->makeValid('required')->save();
        $value = (float) $request->post('value')->makeValid('required')->save();

        if(!$request->getValid())
            return Response::error($this->error_invalid_form, 500);

        try{    
            $db = new DB;

            $stmt = $db
            ->query("
                UPDATE sensor_details
                SET value = ?
                WHERE id = ?
                AND user_id = ?
                AND is_control = '1'
            ")
            ->param([
                $value,
                $sensor_id,
                $this->getCurrentUser()
            ])
            ->send();

            $success = true;
        } catch (\Exception $e) {
            $success = false;
        }

        if($success){
            if($stmt->rowCount() > 0){
                return Response::json(['message' => 'data is updated']);
            }

            return Response::json(['message' => 'no data is updated']);
        }
        else{
            return Response::error('failed', 500);
        }
    }
}