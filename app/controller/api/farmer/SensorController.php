<?php 
namespace App\Controller\Api\Farmer;

use App\Controller\Api\Controller;
use App\Controller\Lib\Request;
use App\Controller\Lib\Response;
use App\Controller\Lib\Token;
use App\Model\DB;

/**
* 
*/
class SensorController extends Controller
{
	
	public function __construct(){
        $this->checkAuth(['status' => 'a']);
	}


    public function getSensor(){
        if(!$this->isAuth())
            return Response::error($this->error_invalid_token, 401);
        else if(!$this->checkPermit(['status' => ['a', 'f']]))
            return Response::error($this->error_not_permit, 500);

        try{    
            $db = new DB;

            $stmt = $db
            ->query("
                SELECT * FROM sensors
            ")
            ->send();
            
            while($data = $db->fetchObj($stmt)){
                $sensor[] = [
                    'id' => $data->id,
                    'name' => $data->name,
                    'description' => $data->description,
                ];
            }

            $success = true;
        } catch (\Exception $e) {
            $success = false;
        }

        if($success){
            if($stmt->rowCount() > 0){
                return Response::json($sensor);
            }

            return Response::error('no data found', 404);
        }
        else{
            return Response::error('failed', 500);
        }
    }

    public function setSensor(Request $request){
        if(!$this->isAuth())
            return Response::error($this->error_invalid_token, 401);
        else if(!$this->isPermit())
            return Response::error($this->error_not_permit, 500);

        $id = (int) $request->post('id')->makeValid('required')->save();
        $name = $request->post('name')->makeValid('required')->save();
        $description = $request->post('description')->save();
        
        if(!$request->getValid())
            return Response::error($this->error_invalid_form, 500);

        try{    
            $db = new DB;

            $stmt = $db
            ->query("
                UPDATE sensors
                SET name = ?,
                    description = ?
                WHERE id = ?
            ")
            ->param([
                $name,
                $description,
                $id
            ])
            ->send();

            $success = true;
        } catch (\Exception $e) {
            $success = false;
        }

        if($success){
            if($stmt->rowCount() > 0){
                return Response::json(['message' => 'data is updated']);
            }

            return Response::json(['message' => 'no data is updated']);
        }
        else{
            return Response::error('failed', 500);
        }
    }

    public function storeSensor(Request $request){
        if(!$this->isAuth())
            return Response::error($this->error_invalid_token, 401);
        else if(!$this->isPermit())
            return Response::error($this->error_not_permit, 500);

        $name = $request->post('name')->makeValid('required')->save();
        $description = $request->post('description')->save();
        
        if(!$request->getValid())
            return Response::error($this->error_invalid_form, 500);

        try{
            $db = new DB;

            $stmt = $db
            ->query("
                INSERT INTO sensors (
                    name,
                    description,
                )
                VALUES (
                    ?,?
                )
            ")
            ->param([
                $name,
                $description
            ])
            ->send();

            $success = true;
        } catch (\Exception $e) {
            $success = false;
        }

        if($success){
            return Response::created();
        }
        else{
            return Response::error('failed create new sensor', 500);
        }
    }

    public function deleteSensor(Request $request){
        if(!$this->isAuth())
            return Response::error($this->error_invalid_token, 401);
        else if(!$this->isPermit())
            return Response::error($this->error_not_permit, 500);

        $id = (int) $request->post('id')->makeValid('required')->save();
        
        if(!$request->getValid())
            return Response::error($this->error_invalid_form, 500);

        try{    
            $db = new DB;

            $stmt = $db
            ->query("
                DELETE FROM sensors 
                WHERE id=?
            ")
            ->param([
                $id
            ])
            ->send();
            
            $success = true;
        } catch (\PDOException $e) {
            $success = false;
        }

        if($success){
            if($stmt->rowCount() > 0){
                Token::delete($this->token);
                return Response::json(['message' => 'sensor is deleted']);
            }

            return Response::json(['message' => 'no sensor is deleted']);
        }
        else{
            return Response::error('failed', 500);
        }
    }


    public function getSensorDetail(Request $request){
        if(!$this->isAuth())
            return Response::error($this->error_invalid_token, 401);
        else if(!$this->checkPermit(['status' => ['a', 'f']]))
            return Response::error($this->error_not_permit, 500);

        $action = $request->param('action')->save();

        //check, admin or not
        $currentUser = ($this->getCurrentStatus() == 'a') ?
             (int) $request->param('farmer_id')->save() :
             $this->getCurrentUser();

        $is_control = '%';
        if($action == 'view')
            $is_control = '%0%';
        else if($action == 'control')
            $is_control = '%1%';

        try{
            $db = new DB;

            $stmt = $db
            ->query("
                SELECT
                    sensor_details.*,
                    DATE_FORMAT(updated_at, '%d/%m/%Y, %h:%i WIB') AS last_updated,
                    sensor_details.name AS detail_name,
                    sensors.name AS sensor_name
                FROM sensor_details
                JOIN sensors
                ON sensor_details.sensor_id = sensors.id
                WHERE sensor_details.user_id = ?
                AND is_control LIKE ?
            ")
            ->param([
                $currentUser,
                $is_control
            ])
            ->send();

            while($data = $db->fetchObj($stmt)){
                $sensor[$data->sensor_id]['sensor_id'] = $data->sensor_id;
                $sensor[$data->sensor_id]['sensor_name'] = $data->sensor_name;
                $sensor[$data->sensor_id]['user_id'] = $data->user_id;
                
                $sensor[$data->sensor_id]['detail'][] = [
                    'id' => $data->id,
                    'name' => $data->detail_name,
                    'value' => $data->value,
                    'is_control' => $data->is_control,
                    'updated_at' => $data->last_updated,
                    'secret_code' => $data->secret_code
                ];
            }

            $success = true;
        } catch (\Exception $e) {
            $success = false;
        }

        if($success){
            if($stmt->rowCount() > 0){
                foreach ($sensor as $key) {
                    $result[] = $key;
                }
                return Response::json($result);
            }

            return Response::error('no data found', 404);
        }
        else{
            return Response::error('failed', 500);
        }
    }

    
    public function storeSensorDetail(Request $request){
        if(!$this->isAuth())
            return Response::error($this->error_invalid_token, 401);
        else if(!$this->checkPermit(['status' => 'f']))
            return Response::error($this->error_not_permit, 500);

        $name = $request->post('name')->makeValid('required')->save();
        $sensor_id = $request->post('sensor_id')->makeValid('required')->save();
        
        if(!$request->getValid())
            return Response::error($this->error_invalid_form, 500);

        try{
            $secret_code = $this->uniqRandom();

            $db = new DB;

            $stmt = $db
            ->query("
                INSERT INTO sensor_details (
                    name,
                    sensor_id,
                    secret_code,
                    user_id
                )
                VALUES (
                    ?,?,?,?
                )
            ")
            ->param([
                $name,
                $sensor_id,
                $secret_code,
                $this->getCurrentUser()
            ])
            ->send();
            
            $success = true;
        } catch (\Exception $e) {
            $success = false;
        }

        if($success){
            return Response::created();
        }
        else{
            return Response::error('failed create new sensor', 500);
        }
    }

    public function deleteSensorDetail(Request $request){
        if(!$this->isAuth())
            return Response::error($this->error_invalid_token, 401);
        else if(!$this->checkPermit(['status' => 'f']))
            return Response::error($this->error_not_permit, 500);

        $id = (int) $request->post('id')->makeValid('required')->save();
        
        if(!$request->getValid())
            return Response::error($this->error_invalid_form, 500);

        try{    
            $db = new DB;

            $stmt = $db
            ->query("
                DELETE FROM sensor_details 
                WHERE id=?
                AND user_id=?
            ")
            ->param([
                $id,
                $this->getCurrentUser()
            ])
            ->send();
            
            $success = true;
        } catch (\PDOException $e) {
            $success = false;
        }

        if($success){
            if($stmt->rowCount() > 0){
                Token::delete($this->token);
                return Response::json(['message' => 'your sensor is deleted']);
            }

            return Response::json(['message' => 'no your sensor is deleted']);
        }
        else{
            return Response::error('failed', 500);
        }
    }

    public function updateValue(Request $request){
        $id = (int) $request->param('id')->save();
        $secret_code = $request->param('secret_code')->save();
        $value = (float) $request->get('value')->makeValid('required')->save();

        if(!$request->getValid())
            return Response::error($this->error_invalid_form, 500);

        try{    
            $db = new DB;

            $stmt = $db
            ->query("
                UPDATE sensor_details
                SET value = ?
                WHERE id = ?
                AND secret_code = ?
            ")
            ->param([
                $value,
                $id,
                $secret_code
            ])
            ->send();

            $success = true;
        } catch (\Exception $e) {
            $success = false;
        }

        if($success){
            if($stmt->rowCount() > 0){
                return Response::json(['message' => 'value is updated']);
            }

            return Response::json(['message' => 'no value is updated']);
        }
        else{
            return Response::error('failed', 500);
        }
    }

    public function takeValue(Request $request){
        $id = (int) $request->param('id')->save();
        $secret_code = $request->param('secret_code')->save();

        try{    
            $db = new DB;

            $stmt = $db
            ->query("
                SELECT id, sensor_id, value
                FROM sensor_details
                WHERE id = ?
                AND secret_code = ?
            ")
            ->param([
                $id,
                $secret_code
            ])
            ->send();

            while($data = $db->fetchObj($stmt)){
                $sensor = [
                    'id' => $data->id,
                    'sensor' => $data->sensor_id,
                    'value' => $data->value,
                ];
            }

            $success = true;
        } catch (\Exception $e) {
            $success = false;
        }

        if($success){
            if($stmt->rowCount() > 0){
                return Response::json($sensor);
            }

            return Response::error('no data found', 404);
        }
        else{
            return Response::error('failed', 500);
        }
    }
}