<?php 
namespace App\Controller\Api\User;

use App\Controller\Api\Controller;
use App\Controller\Lib\Request;
use App\Controller\Lib\Response;
use App\Controller\Lib\Token;
use App\Model\DB;

/**
* 
*/
class UserController extends Controller
{
	public function __construct(){
        $this->checkAuth();
	}

    public function getDetail(){
        if(!$this->isAuth())
            return Response::error($this->error_invalid_token, 401);
        else if(!$this->isPermit())
            return Response::error($this->error_not_permit, 500);

        try{    
            $db = new DB;

            $stmt = $db
            ->query("
                SELECT * FROM users
                WHERE id=?
            ")
            ->param([
                $this->getCurrentUser()
            ])
            ->send();
    
            while($data = $db->fetchObj($stmt)){
                $detail = [
                    'id' => $data->id,
                    'name' => $data->name,
                    'email' => $data->email,
                    'status' => $data->status,
                    'created_at' => $data->created_at,
                    'updated_at' => $data->updated_at,
                ];
            }

            $success = true;
        } catch (\Exception $e) {
            $success = false;
        }

        if($success){
            if($stmt->rowCount() > 0){
                return Response::json($detail);
            }

            return Response::error('no data found', 404);
        }
        else{
            return Response::error('failed', 500);
        }
    }

    public function update(Request $request){
        if(!$this->isAuth())
            return Response::error($this->error_invalid_token, 401);
        else if(!$this->isPermit())
            return Response::error($this->error_not_permit, 500);
        
        $name = $request->post('name', true)->makeValid('required')->save();
        $email = $request->post('email')->makeValid('email|required')->save();
        $old_password = $request->post('old_password')->makeValid('required')->save();
        $new_password = $request->post('new_password')->makeValid('required')->save();

        if(!$request->getValid())
            return Response::error($this->error_invalid_form, 500);

        try{
            $new_password = $this->hashPassword($new_password);
            $old_password = $this->hashPassword($old_password);
            
            $db = new DB;
            
            $stmt = $db
            ->query("
                UPDATE users
                SET name=?,
                    email=?,
                    password=?
                WHERE id=?
                AND password=?
            ")
            ->param([
                $name,
                $email,
                $new_password,
                $this->getCurrentUser(),
                $old_password
            ])
            ->send();

            $success = true;
        } catch (\PDOException $e) {
            $success = false;
        }

        if($success){
            if($stmt->rowCount() > 0){
                return Response::json(['message' => 'data is updated']);
            }

            return Response::json(['message' => 'no data is updated']);
        }
        else{
            return Response::error('error, email is used another one', 500);
        }
    }

    public function delete(Request $request){
        if(!$this->isAuth())
            return Response::error($this->error_invalid_token, 401);
        else if(!$this->isPermit())
            return Response::error($this->error_not_permit, 500);

        $password = $request->post('password')->makeValid('required')->save();
        
        if(!$request->getValid())
            return Response::error($this->error_invalid_form, 500);

        try{    
            $password = $this->hashPassword($password);

            $db = new DB;

            $stmt = $db
            ->query("
                DELETE FROM users 
                WHERE id=?
                AND password=?
            ")
            ->param([
                $this->getCurrentUser(),
                $password
            ])
            ->send();
            
            $success = true;
        } catch (\PDOException $e) {
            $success = false;
        }

        if($success){
            if($stmt->rowCount() > 0){
                Token::delete($this->getCurrentToken());
                return Response::json(['message' => 'data is deleted']);
            }

            return Response::json(['message' => 'no data is deleted']);
        }
        else{
            return Response::error('failed', 500);
        }
    }
}