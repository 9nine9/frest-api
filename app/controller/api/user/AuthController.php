<?php 
namespace App\Controller\Api\User;

use App\Controller\Api\Controller;
use App\Controller\Lib\Request;
use App\Controller\Lib\Response;
use App\Controller\Lib\Token;
use App\Model\DB;

/**
* 
*/
class AuthController extends Controller
{
    public function __construct(){
        $this->checkAuth();
    }

    public function login(Request $request){
        $email = $request->post('email')->makeValid('email|required')->save();
        $password = $request->post('password')->makeValid('required')->save();
        
        if(!$request->getValid())
            return Response::error($this->error_invalid_form, 500);

        try{
            $password = $this->hashPassword($password);
            
            $db = new DB;
            $stmt = $db
            ->query("
                SELECT id, status, is_active FROM users 
                WHERE email=?
                AND password=?
            ")
            ->param([
                $email,
                $password
            ])
            ->send();

            while($data = $db->fetchObj($stmt)){
                $user = [
                    'id' => $data->id,
                    'status' => $data->status,
                    'is_active' => $data->is_active
                ];
            }

            $success = ($stmt->rowCount() == 1) ? true : false;
        } catch (\Exception $e) {
            $success = false;
        }

        if($success){
            $token = Token::create($user);
            return Response::json([
                'token' => $token,
                'status' => $user['status'],
                'is_active' => $user['is_active']
            ]);
        }
        else{
            return Response::error('email or password is invalid', 401);
        }
    }

	public function register(Request $request){
        $email = $request->post('email')->makeValid('email|required')->save();
        $password = $request->post('password')->makeValid('required')->save();
        $name = $request->post('name', true)->makeValid('required')->save();
        $status = $request->post('status')->makeValid('required')->save();
        
        if(!$request->getValid())
            return Response::error($this->error_invalid_form, 500);

        try{
            $password = $this->hashPassword($password);
            $code_activate = $this->uniqRandom();
            
            $db = new DB;

            $stmt = $db
            ->query("
                INSERT INTO users (
                    email,
                    password,
                    name,
                    status,
                    code_activate
                )
                VALUES (
                    ?,?,?,?,?
                )
            ")
            ->param([
                $email,
                $password,
                $name,
                $status,
                $code_activate
            ])
            ->send();
            
            $success = true;
        } catch (\Exception $e) {
            //return Response::error($e->getMessage(), $e->getCode());
            $success = false;
        }

        if($success){
            return Response::created();
        }
        else{
            return Response::error('failed create new account', 500);
        }
    }

    public function checkToken(){
        if(!$this->isAuth())
            return Response::error($this->error_invalid_token, 401);
        else if(!$this->isPermit())
            return Response::error($this->error_not_permit, 500);
        
        $new_token = Token::update($this->getCurrentToken());
        Token::delete($this->getCurrentToken());

        return Response::json(['new_token' => $new_token]);
    }

    public function logout(){
        if(!$this->isAuth())
            return Response::error($this->error_invalid_token, 401);
        else if(!$this->isPermit())
            return Response::error($this->error_not_permit, 500);

        if(Token::delete($this->getCurrentToken())){
            return Response::json(['message' => 'token have been blacklisted']);
        }
        else{
            return Response::error('failed blacklist the token', 500);
        }

    }
}