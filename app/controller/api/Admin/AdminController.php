<?php 
namespace App\Controller\Api\Admin;

use App\Controller\Api\Controller;
use App\Controller\Lib\Request;
use App\Controller\Lib\Response;
use App\Controller\Lib\Token;
use App\Model\DB;

/**
* 
*/
class AdminController extends Controller
{
	public function __construct(){
        $this->checkAuth(['status' => 'a']);
	}


	public function getFarmer(Request $request){
		if(!$this->isAuth())
            return Response::error($this->error_invalid_token, 401);
        else if(!$this->isPermit())
            return Response::error($this->error_not_permit, 500);

        $currentFarmer = (int) $request->param('id')->save();
        $filter = '';
        if($currentFarmer){
        	$filter = 'AND id = ?';
        }
        	

        try{    
            $db = new DB;

            $stmt = $db
            ->query("
                SELECT * FROM users
                WHERE status = 'f'
                $filter
            ")
            ->param([
                $currentFarmer
            ])
            ->send();
    
            while($data = $db->fetchObj($stmt)){
                $farmer[] = [
                    'id' => $data->id,
                    'name' => $data->name,
                    'email' => $data->email,
                    'status' => $data->status,
                    'created_at' => $data->created_at,
                    'updated_at' => $data->updated_at,
                ];
            }

            $success = true;
        } catch (\Exception $e) {
            $success = false;
        }

        if($success){
            if($stmt->rowCount() > 0){
                return Response::json($farmer);
            }

            return Response::error('no data found', 404);
        }
        else{
            return Response::error('failed', 500);
        }
	}
}