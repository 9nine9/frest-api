<?php
namespace App\Controller\Lib;

class Response
{	
	/**
     * build
     * 
     * @return void
     */
    public function __construct(){
    }    

    /**
     * Error Response When Execute
     * 
     * @return json
     */
    public static function error($message, $code){
        http_response_code($code);
        $response['code'] = $code;
        $response['message'] = $message;
        $response['data'] = null;

        return json_encode($response);
    }

    /**
     * OK and JSON Response When Execute
     * 
     * @return json
     */
    public static function json($data){
        $response['code'] = 200;
        $response['message'] = 'OK';
        $response['data'] = (array) $data;

        return json_encode($response);
    }

    /**
     * Created Response When Execute
     * 
     * @return void
     */
    public static function created(){
        http_response_code(201);
        $response['code'] = 201;
        $response['message'] = 'CREATED';
        $response['data'] = null;

        return json_encode($response);
    }
}