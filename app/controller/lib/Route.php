<?php
namespace App\Controller\Lib;

/**
* 
*/
class Route
{	
	public $route = array();

	public function get($uri, $method = null, $param = null){
		$request = 'GET';
		$this->create($request, $uri, $method, $param);
	}

	public function post($uri, $method = null, $param = null){
		$request = 'POST';
		$this->create($request, $uri, $method, $param);
	}

	public function add($uri, $method = null, $param = null){
		$request = array('GET', 'POST');
		$this->create($request, $uri, $method, $param);
	}

	private function create($request, $uri, $method = null, $param = null){
		$this->route[] = array(
			'request' => $request,
			'uri' => $uri,
			'method' =>	$method,
			'param' => (array) $param
		);
	}	

}
