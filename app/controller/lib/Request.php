<?php
namespace App\Controller\Lib;

use App\Controller\Lib\CommonControl;

class Request extends CommonControl
{	
    private $data_params;
    private $data;
    private $is_valid = true;

	/**
     * build
     * 
     * @return void
     */
    public function __construct(){
    }    

    
    /**
     * Input Param
     * 
     * @return void
     */
    public function input($string){
        $this->data_params = (array) $this->filterForm($string);
    }

    /**
     * Param Request Form
     * 
     * @return this
     */
    public function param($string){
        $this->data = isset($this->data_params[$string]) ? $this->data_params[$string] : '';
        return $this;
    }

    /**
     * Post Request Form
     * 
     * @return this
     */
    public function post($string, $upper = false){
        $this->data = isset($_POST[$string]) ? $this->filterForm($_POST[$string], $upper) : '';
        return $this;
    }

    /**
     * Get Request Form
     * 
     * @return this
     */
    public function get($string, $upper = false){
        $this->data = isset($_GET[$string]) ? $this->filterForm($_GET[$string], $upper) : '';
        return $this;
    }

    /**
     * Save Request Form
     * 
     * @return string
     */
    public function save(){
        return $this->data;
    }    

    /**
     * is post method when request 
     * 
     * @return boolean
     */
    public function isPostMethod(){
        return ($_SERVER["REQUEST_METHOD"] == "POST");
    }

    public function makeValid($string){
        /* email|url|required */
        
        if($this->is_valid){
            $error = false;

            $validation = explode('|', $string);

            foreach ($validation as $key => $value) {
                switch ($value) {
                    case 'required':
                        $error = $this->isEmpty($this->data);
                        break;
                    case 'email':
                        $error = !$this->isMail($this->data);
                        break;
                    case 'url':
                        $error = !$this->isURL($this->data);
                        break;
                    default:
                        # code...
                        break;
                }
                
                if($error){
                    $this->is_valid = false;
                    break;
                }
            }

        }

        return $this;
    }

    public function getValid(){
        return $this->is_valid;
    }

    public function setValid($bool){
        $this->is_valid = $bool;
    }
}