<?php
$web = new App\Controller\Lib\Route;

$web->get('/account/login', '/App/Controller/Auth/login.html');
$web->get('/farmer/dashboard', '/App/Controller/Farmer/home.html');
$web->get('/admin/dashboard', '/App/Controller/Admin/home.html');

/**
 * create routing from Klein with class Route and 'render' function
 *
 */
foreach ($web->route as $key) {
	$this->respond($key['uri'], function ($request, $response, $service) use ($key) {
		$filename = _CONFIG_['path']['web'] . $key['method'];
		$service->render($filename, $key['param']);
	});
}