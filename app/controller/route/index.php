<?php
require_once '../autoloader.php';

/**
 * create group of routing
 * example, group 'api', so url must be api/contoh/contoh1
 * 
 */
$project = _CONFIG_['route'];

$request = Klein\Request::createFromGlobals();
$request->server()->set('REQUEST_URI', substr($_SERVER['REQUEST_URI'],  strlen(_CONFIG_['path']['app'])));
$klein = new Klein\Klein();


$klein->with("", "Web.php");
foreach($project as $route) {
	$prefix = strtolower($route);
    // Include all routes defined in a file under a given namespace
    $klein->with("/$prefix", "$route.php");
}

$klein->dispatch($request);




/**
 * run method 
 * $method = 'ClassName@MethodName'
 * $param = [$arg1, $arg2, $arg3]
 * $response,
 *
 * @return response method (ex. json)
 */
function run ($request, $response, $route = array()){
	header('Content-type: application/json');
	$result = null;
	$method = (isset($route['method'])) ? $route['method'] : null;
	$param = (isset($route['param'])) ? $route['param'] : null;

	/**
	 * check, if ->param(1) is not null, put all ->params() (parameter in url) to variable $param[0]
	 * index 'number' in param(number) is there parameter in url, like id, value, secret_code
	 * example, /sensor/[i:id]/value/update/[a:value]/[a:secret_code]
	 */
	if($request->param(1) != null && $param[0] != null){
		$param[0]->input($request->params());
	}

	if(is_string($method)){
		$method_value = explode('@', $method);
				
		$class = $method_value[0];
		$obj = new $class;

		if(count($method_value) > 1){
			$function = $method_value[1];
			$result = $obj->$function(...(array) $param);
		}

		$response->code(http_response_code());
	}

	else if($method != null) $result = call_user_func($method);

	return $result;
}