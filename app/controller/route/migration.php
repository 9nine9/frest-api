<?php
use App\Controller\Lib\Request;

$migration = new App\Controller\Lib\Route;

$migration->get('/refresh/[a:secret_code]', 'App\Database\MigrateController@refresh');
$migration->get('/create/[a:secret_code]', 'App\Database\MigrateController@create');

/**
 * create routing from Klein with class Route and 'run' function
 *
 */
foreach ($migration->route as $key) {
	$this->respond($key['request'], $key['uri'], 
		function ($request, $response, $service) use ($key) {
			return run ($request, $response, $key);
		}
	);	
}
