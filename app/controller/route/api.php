<?php
$req = new App\Controller\Lib\Request;
$api = new App\Controller\Lib\Route;

//$api->get('/auth/cek', 'App\Controller\Api\User\Contoh@cek');

$api->post('/auth/login', 'App\Controller\Api\User\AuthController@login', [$req]);
$api->post('/auth/register', 'App\Controller\Api\User\AuthController@register', [$req]);
$api->get('/auth/token', 'App\Controller\Api\User\AuthController@checkToken');
$api->get('/auth/logout', 'App\Controller\Api\User\AuthController@logout');

$api->post('/user/update', 'App\Controller\Api\User\UserController@update', [$req]);
$api->post('/user/delete', 'App\Controller\Api\User\UserController@delete', [$req]);
$api->get('/user/data', 'App\Controller\Api\User\UserController@getDetail');

$api->get('/sensor/get', 'App\Controller\Api\Farmer\SensorController@getSensor');
$api->post('/sensor/set', 'App\Controller\Api\Farmer\SensorController@setSensor', [$req]);
$api->post('/sensor/create', 'App\Controller\Api\Farmer\SensorController@storeSensor', [$req]);
$api->post('/sensor/delete', 'App\Controller\Api\Farmer\SensorController@deleteSensor', [$req]);
$api->get('/sensor/detail/get/[control|view:action]?', 'App\Controller\Api\Farmer\SensorController@getSensorDetail', [$req]);
$api->post('/sensor/detail/create', 'App\Controller\Api\Farmer\SensorController@storeSensorDetail', [$req]);
$api->post('/sensor/detail/delete', 'App\Controller\Api\Farmer\SensorController@deleteSensorDetail', [$req]);

//admin
$api->get('/admin/farmer/[i:id]?/get', 'App\Controller\Api\Admin\AdminController@getFarmer', [$req]);
$api->get('/sensor/detail-farmer/[i:farmer_id]/get/[control|view:action]?', 'App\Controller\Api\Farmer\SensorController@getSensorDetail', [$req]);


$api->get('/sensor/[i:id]/update/[a:secret_code]', 'App\Controller\Api\Farmer\SensorController@updateValue', [$req]); //?value=
$api->get('/sensor/[i:id]/take/[a:secret_code]', 'App\Controller\Api\Farmer\SensorController@takeValue', [$req]);

$api->post('/farmer/sensor/value/update', 'App\Controller\Api\Farmer\FarmerController@updateSensorValue', [$req]);



/**
 * create routing from Klein with class Route and 'run' function
 *
 */
foreach ($api->route as $key) {
	$this->respond($key['request'], $key['uri'], 
		function ($request, $response, $service) use ($key) {
			return run ($request, $response, $key);
		}
	);	
}
