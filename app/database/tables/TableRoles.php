<?php
namespace App\Database\Tables;

use App\Database\Tables\Tables;

class TableRoles extends Tables
{

	public function __construct($table){
		$this->tb_name = $table;
	}

	public function create(){
		$this->action .= "| CREATE |";

		$this->query .= "
			CREATE TABLE `$this->tb_name` (
	  			`status` char(1) NOT NULL,
			  	`description` varchar(191) NOT NULL
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;
		";
		
		return $this;
		
	}

	public function index(){
		$this->action .= "| INDEX |";

		$this->query .= "
			ALTER TABLE `$this->tb_name`
	  		ADD PRIMARY KEY (`status`);
		";

		return $this;
	}

	public function feed(){
		$this->action .= "| FEED |";

		$this->query .= "
			INSERT INTO `$this->tb_name` 
			(`status`, `description`) VALUES
			('a', 'Admin'),
			('f', 'Farmer'),
			('c', 'Customer');
		";

		return $this;
	}

}