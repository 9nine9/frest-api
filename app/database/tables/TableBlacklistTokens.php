<?php
namespace App\Database\Tables;

use App\Database\Tables\Tables;

class TableBlacklistTokens extends Tables
{

	public function __construct($table){
		$this->tb_name = $table;
		$this->getDBConnect();
	}

	public function create(){
		$this->action .= "| CREATE |";

		$this->query .= "
			CREATE TABLE `$this->tb_name` (
	  			`id` int(10) UNSIGNED NOT NULL,
			  	`token` varchar(191) NOT NULL,
			  	`expired_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;
		";
		
		return $this;
		
	}

	public function index(){
		$this->action .= "| INDEX |";

		$this->query .= "
			ALTER TABLE `$this->tb_name`
	  		ADD PRIMARY KEY (`id`),
	  		ADD UNIQUE KEY `token` (`token`),

	  		MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
		";

		return $this;
	}

	public function feed(){
		
		return $this;
	}

}