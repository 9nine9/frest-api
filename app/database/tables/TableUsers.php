<?php
namespace App\Database\Tables;

use App\Database\Tables\Tables;

class TableUsers extends Tables
{
	public function __construct($table){
		$this->tb_name = $table;
	}

	public function create(){
		$this->action .= "| CREATE |";

		$this->query .= "
			CREATE TABLE `$this->tb_name` (
			  `id` int(10) UNSIGNED NOT NULL,
			  `email` varchar(191) NOT NULL,
			  `password` varchar(191) NOT NULL,
			  `name` varchar(191) NOT NULL,
			  `status` char(1) NOT NULL DEFAULT 'f',
			  `code_activate` varchar(191) NOT NULL,
			  `is_active` tinyint(1) NOT NULL DEFAULT '0',
			  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;
		";
		
		return $this;
	}

	public function index(){
		$this->action .= "| INDEX |";

		$this->query .= "
			ALTER TABLE `$this->tb_name`
			  ADD PRIMARY KEY (`id`),
			  ADD UNIQUE KEY `email` (`email`),
			  ADD KEY `rel_status` (`status`),

			  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
		";

		return $this;
	}

	public function feed(){
		$this->action .= "| FEED |";

		$this->query .= "
			INSERT INTO `$this->tb_name` 
			(`id`, `email`, `password`, `name`, `status`, `code_activate`, `is_active`, `created_at`, `updated_at`) VALUES
			(37, 'farmer@frest.id', 'bae87ad7e89d6592e47771f89d92e0621ec37b12', 'SANTOS', 'f', '1aea8c7084f708326b01e62fe7cb32a411a8ff25', 1, '2018-01-06 17:00:00', '2018-01-10 13:45:59'),
			(38, 'farmer2@frest.id', 'bae87ad7e89d6592e47771f89d92e0621ec37b12', 'SANTOS W', 'f', '96510109d4653dcd196d56a707abcacfe87627b4', 1, '2018-01-06 17:00:00', '2018-01-10 14:23:43'),
			(39, 'user@frest.id', 'bae87ad7e89d6592e47771f89d92e0621ec37b12', 'SANTOS K', 'c', '481760e30ad14826eee2c2a3a85ff9ce1fc0c03a', 1, '2018-01-06 17:00:00', '2018-01-10 13:18:27');
			(40, 'admin@frest.id', 'bae87ad7e89d6592e47771f89d92e0621ec37b12', 'ADMIN', 'a', '481760e30ad14826eee2c2a3a85ff9ce1fc0c03a', 1, '2018-01-06 17:00:00', '2018-01-10 13:18:27');
		";

		return $this;
	}
}