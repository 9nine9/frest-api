<?php
namespace App\Database\Tables;

use App\Database\Tables\Tables;

class TableSensorDetails extends Tables
{
	public function __construct($table){
		$this->tb_name = $table;
	}

	public function create(){
		$this->action .= "| CREATE |";

		$this->query .= "
			CREATE TABLE `$this->tb_name` (
			  `id` int(10) UNSIGNED NOT NULL,
			  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
			  `sensor_id` int(10) UNSIGNED NOT NULL,
			  `value` float NOT NULL DEFAULT '0',
			  `name` varchar(191) NOT NULL,
			  `secret_code` varchar(191) DEFAULT NULL,
			  `is_control` tinyint(1) NOT NULL DEFAULT '0',
			  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;
		";

		return $this;
		
	}

	public function index(){
		$this->action .= "| INDEX |";

		$this->query .= "
			ALTER TABLE `$this->tb_name`
			  ADD PRIMARY KEY (`id`),
			  ADD KEY `rel_user` (`user_id`),
			  ADD KEY `rel_sensor` (`sensor_id`),

			  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
		";
		
		return $this;
	}

	public function feed(){
		$this->action .= "| FEED |";

		$this->query .= "
			INSERT INTO `$this->tb_name` 
			(`id`, `user_id`, `sensor_id`, `value`, `name`, `secret_code`, `created_at`, `updated_at`) VALUES
			(1, 37, 1, 5, 'pH Tanaman A', 'abc', '2018-01-08 13:01:05', '2018-01-08 15:42:23'),
			(2, 38, 1, 10, 'pH Tanaman B', 'abc', '2018-01-08 13:01:05', '2018-01-09 18:43:01'),
			(3, 38, 3, 0, 'Oke Deh', '561437ba268feac93bdabae543c5cf22193fd2af', '2018-01-08 13:01:34', '2018-01-10 14:29:09'),
			(4, 39, 3, 0, 'Oke Fak', '561437ba268feac93bdabae543c5cf22193fd2af', '2018-01-08 13:01:34', '2018-01-10 14:29:05'),
			(5, 38, 1, 2, 'Burnguu', '561437ba268feac93bdabae543c5cf22193fd2af', '2018-01-08 15:58:12', '2018-01-10 06:06:31');
		";

		return $this;
	}

}