<?php
namespace App\Database\Tables;

use App\Database\Tables\Tables;

class TableSensors extends Tables
{
	public function __construct($table){
		$this->tb_name = $table;
	}

	public function create(){
		$this->action .= "| CREATE |";

		$this->query .= "
			CREATE TABLE `$this->tb_name` (
			 	`id` int(10) UNSIGNED NOT NULL,
  				`name` varchar(191) NOT NULL,
  				`description` text NOT NULL
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;
		";

		return $this;
		
	}

	public function index(){
		$this->action .= "| INDEX |";

		$this->query .= "
			ALTER TABLE `$this->tb_name`
 				ADD PRIMARY KEY (`id`),
			  	MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
		";
		
		return $this;
	}

	public function feed(){
		$this->action .= "| FEED |";

		$this->query .= "
			INSERT INTO `$this->tb_name` 
			(`id`, `name`, `description`) VALUES
			(1, 'pH', 'Untuk mengukur derajat keasaman atau kebasaan pH tanaman'),
			(2, 'EC', 'Untuk mengukur kepekatan suatu larutan (dalam hal ini adalah larutan nutrisi tanaman). EC mengukur nilai konduktivitasnya'),
			(3, 'moisture', 'Untuk mengukur kelembaban tanah'),
			(4, 'temperature', 'Untuk mengukur suhu'),
			(5, 'ultrasonic', 'Untuk mengukur jarak (dalam hal ini untuk mengukur ketinggian air di dalam bak)'),
			(6, 'electricity', 'Untuk mengukur arus listrik (dalam hal ini untuk mengukur listrik pada sel surya)');

		";

		return $this;
	}

}