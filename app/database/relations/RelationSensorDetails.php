<?php
namespace App\Database\Relations;

use App\Database\Relations\Relations;

class RelationSensorDetails extends Relations
{
	public function __construct($table){
		$this->tb_name = $table;
	}

	public function relate(){
		$this->action .= "| RELATE |";

		$this->query .= "
			ALTER TABLE `$this->tb_name`
			  ADD CONSTRAINT `rel_sensor` FOREIGN KEY (`sensor_id`) REFERENCES `sensors` (`id`) ON UPDATE CASCADE,
			  ADD CONSTRAINT `rel_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

		";
		
		return $this;
	}

}