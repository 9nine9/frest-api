<?php
namespace App\Database\Relations;

use App\Model\DB;

class Relations
{
	protected $db;
	protected $tb_name;
	protected $query = '';
	protected $action = '';

	public function getDBConnect(){
		$this->db = new DB;
	}

	public function save(){
		$this->getDBConnect();

		try{
			$this->db
			->query("
				$this->query
			")
			->send();

			$success = true;
		} catch (\Exception $e) {
			$success = false;
		}

		if($success){
			return $this->action . ' table ' . $this->tb_name;
		}
		else 
			return 'failed ' . $this->action . ' table ' . $this->tb_name; 

		return $success;
	}

	
	public function getDBClose(){
		$this->db = null;
	}
}