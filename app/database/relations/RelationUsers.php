<?php
namespace App\Database\Relations;

use App\Database\Relations\Relations;

class RelationUsers extends Relations
{
	public function __construct($table){
		$this->tb_name = $table;
	}

	public function relate(){
		$this->action .= "| RELATE |";

		$this->query .= "
			ALTER TABLE `$this->tb_name`
  			ADD CONSTRAINT `rel_status` FOREIGN KEY (`status`) REFERENCES `roles` (`status`) ON UPDATE CASCADE;
		";
		
		return $this;
	}

}