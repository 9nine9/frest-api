<?php
namespace App\Database;

use App\Database\Tables;
use App\Database\Relations;
use App\Controller\Lib\Request;
use App\Controller\Lib\Response;

class MigrateController
{
	private $tables;
	private $relations;

	public function __construct(){
		$this->tables = [
			new Tables\TableRoles('roles'),
			new Tables\TableUsers('users'),
			new Tables\TableSensors('sensors'),
			new Tables\TableSensorDetails('sensor_details'),
			new Tables\TableBlacklistTokens('blacklist_tokens')
		];

		$this->relations = [
			new Relations\RelationUsers('users'),
			new Relations\RelationSensorDetails('sensor_details')
		];
	}

	public function refresh(){
		if(!$this->isValid())
			return Response::error('oopps!', 500);

		foreach ($this->tables as $table) {
			$result[] = $table->dropIfExist()->create()->index()->feed()->save();
			$table->getDBClose();
		}

		foreach ($this->relations as $relation) {
			$result[] = $relation->relate()->save();
			$relation->getDBClose();
		}

		return Response::json($result);
	}

	public function create(){
		if(!$this->isValid())
			return Response::error('oopps!', 500);

		foreach ($this->tables as $table) {
			$result[] = $table->create()->index()->feed()->save();
			$table->getDBClose();
		}

		foreach ($this->relations as $relation) {
			$result[] = $relation->relate()->save();
			$relation->getDBClose();
		}

		return Response::json($result);
	}


	private function isValid(){
		$request = new Request;
		$secret_code = $request->get('secret_code')->makeValid('required')->save();

        if($request->getValid() && ($secret_code === _CONFIG_['migration']['secret_code']))
            return true;
        else
        	return false;
	}
}