<?php
namespace App\Model;

class DBConnection
{
	public $conn = null;

	/**
     * build
     * 
     * @return object
     */
    public function __construct() {
        if(!$this->conn){
            $dsn = 
                _CONFIG_['db']['driver'].
                ":port=" . _CONFIG_['db']['port'].
                ":host=" . _CONFIG_['db']['host'].
                ";dbname=" . _CONFIG_['db']['dbname']
            ;

            try{
                $this->conn = new \PDO($dsn, _CONFIG_['db']['username'], _CONFIG_['db']['password']);
                $this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            }catch(\PDOException $e){
                echo $e->getMessage();
            }
        }
    }
}