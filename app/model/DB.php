<?php
namespace App\Model;

use App\Model\DBConnection;

class DB
{
    private $conn;
    private $data = array();
    private $query;
    private $param;

    /**
     * build
     * 
     * @return object
     */
    public function  __construct() {
        $this->conn = (new DBConnection)->conn;
    }

    public function close(){
        $this->conn = null;
    }

    public function query($data){
        $this->query = $data;
        return $this;
    }

    public function param($data){
        $this->param = (array) $data;
        return $this;
    }

    public function select($data){
        $this->data['select'] = (isset($this->data['select'])) ?
            array_merge($this->data['select'], (array) $data) : 
            (array) $data;
        
        return $this;
    }

    public function from($data){
        $this->data['from'] = (isset($this->data['from'])) ?
            array_merge($this->data['from'], (array) $data) : 
            (array) $data;

        return $this;
    }

    public function where(){
        $data = func_get_args();
        $this->data['where'] = (isset($this->data['where'])) ?
            array_merge($this->data['where'], (array) $data) : 
            (array) $data;

        return $this;
    }

    public function fetchObj($stmt){
        return $stmt->fetch(\PDO::FETCH_OBJ);
    }

    public function fetchNum($stmt){
        return $stmt->fetch(\PDO::FETCH_NUM);
    }

    public function fetchAssoc($stmt){
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function send(){
        $stmt = null;
        try {
            $stmt = $this->conn->prepare($this->query);
            $stmt->execute((array) $this->param);
        } catch (\PDOException $e) {
            throw new \Exception($e->getMessage(), 500);
        }

        return $stmt;
    }

}