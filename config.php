<?php
define ('_CONFIG_', [
	
	/* -------------------------------
	 * Config time
	 * -------------------------------
	 */
	'timezone' => date_default_timezone_set('Asia/Jakarta'),
	'now' => date("Y-m-d H:i:s"),

	/* -------------------------------
	 * Config token for authefication
	 * -------------------------------
	 */
	'token' => [
		'headers' => [
			'alg' => 'HS256',
			'typ' => 'JWT'
		],

		'expired' => [
			'hours' => 24,
			'minutes' => 0,
			'seconds' => 0,
		],

		'key' => 'secret'

	],

	/* -------------------------------
	 * Config database
	 * -------------------------------
	 */
	'db' => [
		'driver' => 'mysql',
		'host' => 'localhost',
		'port' => '3306',
		'username' => 'root',
		'password' => '',
		'dbname' => 'db_frest'
	],

	/* -------------------------------
	 * Config migration
	 * -------------------------------
	 */
	'migration' => [
		'secret_code' => 'slalala'	
	],

	/* -------------------------------
	 * Config path
	 * -------------------------------
	 */
	'path' => [
		'app' => '/frest',
		'web' => '../../../web',
		'url' => 'http://' . $_SERVER['HTTP_HOST'] . '/frest'
	],

	'route' => [
		'Api',
		'Migration'
	]
]);
